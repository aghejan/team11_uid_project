import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-pharmacist-sidenav',
  template: `
  <div class="full-width border" fxLayout="column" fxLayoutAlign="start center">
  <mat-list>
    <mat-list-item><img src="../../../../assets/mainLogo.png" height="40px"/>New Era Health System</mat-list-item>
  </mat-list>
  <mat-divider class="full-width"></mat-divider>
  <mat-nav-list class="full-width items" fxLayout="column" fxLayoutAlign="start center">
    <mat-list-item (click)="onMedicationPlans()">Medication Plans</mat-list-item>
    <mat-divider class="full-width"></mat-divider>
    <mat-list-item (click)="onCheckStock()">Check Stock</mat-list-item>
    <mat-divider class="full-width"></mat-divider>
  </mat-nav-list>
  <div *ngIf="checkPageUrl()" class="full-width full-height" fxLayout="column" fxLayoutAlign="end center">
      <mat-divider class="full-width"></mat-divider>
      <mat-list class="full-width">
        <mat-list-item fxLayout="row">
          <div fxLayoutAlign="start center">
            Tony Stark
          </div>
          <mat-icon class="cursor-pointer" fxLayoutAlign="end center" (click)="onCloseMedicationPlans()">close</mat-icon>
        </mat-list-item>
      </mat-list>
      <mat-divider class="full-width"></mat-divider>
      <img class="full-width" src="../../../../assets/tony.png"/>
    </div>
</div>
  `,
  styleUrls: ['./pharmacist-sidenav.component.scss']
})
export class PharmacistSidenavComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  
  onCheckStock() {
    this.router.navigate(['stock'], {relativeTo: this.route});
  }

  onMedicationPlans() {
    this.router.navigate(['medicationPlans'], {relativeTo: this.route});

  }

  checkPageUrl() {
    return this.router.url.split('/')[2] === 'medicationPlans';
  }

  onCloseMedicationPlans() {
    this.router.navigate(['pharmacist-platform']);
  }

}
