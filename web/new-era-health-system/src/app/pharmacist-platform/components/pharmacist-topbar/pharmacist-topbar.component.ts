import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pharmacist-topbar',
  template: `
    <div class="main-layout">
      <div class="full-width padding-right-8" fxLayout="row" fxLayoutAlign="end center" fxLayoutGap="8px">
        <div class="mat-title no-margin">
          Welcome, Pharmacist
        </div>
        <mat-icon>account_circle</mat-icon>
      </div>
    </div>
  `,
  styleUrls: ['./pharmacist-topbar.component.scss']
})
export class PharmacistTopbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
