import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PharmacistPlatformComponent} from './containers/pharmacist-platform/pharmacist-platform.component';
import { PharmacistMainDisplayComponent } from './containers/pharmacist-main-display/pharmacist-main-display.component';
import { MedicationPlanComponent } from './containers/medication-plan/medication-plan.component';
import { StockComponent } from './containers/stock/stock.component';
import {AuthenticateGuard} from "../shared/guards/authenticate.guard";

const routes: Routes = [
  {
    path: '',
    component: PharmacistPlatformComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: PharmacistMainDisplayComponent
      },
      {
        path: 'medicationPlans',
        component: MedicationPlanComponent,
        canActivate: [AuthenticateGuard]
      },
      {
        path: 'stock',
        component: StockComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PharmacistPlatformRoutingModule { }
