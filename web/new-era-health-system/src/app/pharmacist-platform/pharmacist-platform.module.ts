import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PharmacistPlatformRoutingModule } from './pharmacist-platform-routing.module';
import { PharmacistPlatformComponent } from './containers/pharmacist-platform/pharmacist-platform.component';
import { PharmacistMainDisplayComponent } from './containers/pharmacist-main-display/pharmacist-main-display.component';
import { PharmacistTopbarComponent } from './components/pharmacist-topbar/pharmacist-topbar.component';
import { PharmacistSidenavComponent } from './components/pharmacist-sidenav/pharmacist-sidenav.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {SharedModule} from "../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MedicationPlanComponent } from './containers/medication-plan/medication-plan.component';
import { StockComponent } from './containers/stock/stock.component';

@NgModule({
  declarations: [PharmacistPlatformComponent, PharmacistMainDisplayComponent, PharmacistTopbarComponent, PharmacistSidenavComponent, MedicationPlanComponent, StockComponent],
  imports: [
    CommonModule,
    PharmacistPlatformRoutingModule,
    SharedModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule
  ]
})
export class PharmacistPlatformModule { }

