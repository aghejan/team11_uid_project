import { Component, OnInit } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export interface Element {
  position: number;
  name: string;
  quantity: number;
  alternative: string;
}

const ELEMENT_DATA1: Element[] = [
  {position: 1, name: 'Paracetamol', quantity: 15, alternative: ''},
  {position: 2, name: 'Nurofen', quantity: 10, alternative: 'Algocalmin'},
];

const ELEMENT_DATA2: Element[] = [
    {position: 1, name: 'NoSpa', quantity: 20, alternative: ''},
    {position: 2, name: 'Triferment', quantity: 10, alternative: ''},
    {position: 3, name: 'Colebil', quantity: 12, alternative: ''}
  ];

@Component({
  selector: 'app-medication-plan',
  template: `
    <div class="bg"></div>
    <div class="main-layout">
      
        <div class="tabs">
            <mat-tab-group> 
                <mat-tab  label="Pneumonia"> 
                    
                    <div class="table">
                        
                        <table mat-table [dataSource]=dataSource1 class="mat-elevation-z8">

                            <!-- Checkbox Column -->
                            
                            <ng-container matColumnDef="select">
                            <th mat-header-cell *matHeaderCellDef>
                            <mat-checkbox (change)="$event ? masterToggle1() : null"
                                [checked]="selection1.hasValue() && isAllSelected1()"
                                [indeterminate]="selection1.hasValue() && !isAllSelected1()"
                                [aria-label]="checkboxLabel1()">
                            </mat-checkbox>
                            </th>
                            <td mat-cell *matCellDef="let row">
                            <mat-checkbox (click)="$event.stopPropagation()"
                                (change)="$event ? selection1.toggle(row) : null"
                                [checked]="selection1.isSelected(row)"
                                [aria-label]="checkboxLabel1(row)">
                            </mat-checkbox>
                            </td>
                            </ng-container>

                            <!-- Position Column -->
                            
                            <ng-container matColumnDef="position">
                            <th mat-header-cell *matHeaderCellDef> No. </th>
                            <td mat-cell *matCellDef="let element"> {{element.position}} </td>
                            </ng-container>

                            <!-- Name Column -->
                            
                            <ng-container matColumnDef="name">
                            <th mat-header-cell *matHeaderCellDef> Name </th>
                            <td mat-cell *matCellDef="let element"> {{element.name}} </td>
                            </ng-container>

                            <!-- Quantity Column -->
                            
                            <ng-container matColumnDef="quantity">
                            <th mat-header-cell *matHeaderCellDef> Quantity </th>
                            <td mat-cell *matCellDef="let element"> {{element.quantity}} </td>
                            </ng-container>

                            <!-- Alternative Column -->
                             
                            <ng-container matColumnDef="alternative">
                            <th mat-header-cell *matHeaderCellDef> Alternative </th>
                            <td mat-cell *matCellDef="let element">  
                            <textarea [matAutosizeMinRows]="1" matInput matTextareaAutosize></textarea> 
                            
                            </td>
                            </ng-container>

                            <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
                            <tr mat-row *matRowDef="let row; columns: displayedColumns;" (click)="selection1.toggle(row)">
                            </tr>
       
                        </table>
                        
                    </div>
                    <button mat-raised-button color="primary" (click)='onDone1()'>Done</button>
  
                </mat-tab>
                <mat-tab  label="Diarrhea"> 
                    
                    <div class="table">
                        
                        <table mat-table [dataSource]=dataSource2 class="mat-elevation-z8">

                            <!-- Checkbox Column -->
                            
                            <ng-container matColumnDef="select">
                            <th mat-header-cell *matHeaderCellDef>
                            <mat-checkbox (change)="$event ? masterToggle2() : null"
                                [checked]="selection2.hasValue() && isAllSelected2()"
                                [indeterminate]="selection2.hasValue() && !isAllSelected2()"
                                [aria-label]="checkboxLabel2()">
                            </mat-checkbox>
                            </th>
                            <td mat-cell *matCellDef="let row">
                            <mat-checkbox (click)="$event.stopPropagation()"
                                (change)="$event ? selection2.toggle(row) : null"
                                [checked]="selection2.isSelected(row)"
                                [aria-label]="checkboxLabel2(row)">
                            </mat-checkbox>
                            </td>
                            </ng-container>

                            <!-- Position Column -->
                            
                            <ng-container matColumnDef="position">
                            <th mat-header-cell *matHeaderCellDef> No. </th>
                            <td mat-cell *matCellDef="let element"> {{element.position}} </td>
                            </ng-container>

                            <!-- Name Column -->
                            
                            <ng-container matColumnDef="name">
                            <th mat-header-cell *matHeaderCellDef> Name </th>
                            <td mat-cell *matCellDef="let element"> {{element.name}} </td>
                            </ng-container>

                            <!-- Quantity Column -->
                            
                            <ng-container matColumnDef="quantity">
                            <th mat-header-cell *matHeaderCellDef> Quantity </th>
                            <td mat-cell *matCellDef="let element"> {{element.quantity}} </td>
                            </ng-container>

                            <!-- Alternative Column -->
                             
                            <ng-container matColumnDef="alternative">
                            <th mat-header-cell *matHeaderCellDef> Alternative </th>
                            <td mat-cell *matCellDef="let element">  
                            <textarea [matAutosizeMinRows]="1" matInput matTextareaAutosize></textarea> 
                            </td>
                            </ng-container>

                            <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
                            <tr mat-row *matRowDef="let row; columns: displayedColumns;" (click)="selection2.toggle(row)">
                            </tr>
       
                        </table>
                        
                    </div>
                    <button mat-raised-button color="primary" (click)='onDone2()'>Done</button>
  
                </mat-tab>

            </mat-tab-group>
        </div>
    </div>
  `,
  styleUrls: ['./medication-plan.component.scss']
})
export class MedicationPlanComponent implements OnInit {

  displayedColumns: string[] = ['select', 'position', 'name', 'quantity', 'alternative'];
  data1 = Object.assign(ELEMENT_DATA1);
  dataSource1 = new MatTableDataSource<Element>(this.data1);
  selection1 = new SelectionModel<Element>(true, []);

  data2 = Object.assign(ELEMENT_DATA2);
  dataSource2 = new MatTableDataSource<Element>(this.data2);
  selection2 = new SelectionModel<Element>(true, []);

  tabs = [
    { label : 'Headache' , dataSource : this.dataSource1, selection: this.selection1, data: this.data1},
    { label : 'Diarrhea' , dataSource : this.dataSource2, selection: this.selection2, data: this.data2},
  ]
  
  constructor() { 
    
  }

  ngOnInit() {
    
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected1() {
    const numSelected = this.selection1.selected.length;
    const numRows = this.dataSource1.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle1() {
    this.isAllSelected1() ?
        this.selection1.clear() :
        this.dataSource1.data.forEach(row => this.selection1.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel1(row?: Element): string {
    if (!row) {
      return `${this.isAllSelected1() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection1.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onDone1() {
    
     this.selection1.selected.forEach(item => {
       let index: number = this.data1.findIndex(d => d === item);
       console.log(this.data1.findIndex(d => d === item));
       this.data1.splice(index,1)
       this.dataSource1 = new MatTableDataSource<Element>(this.data1);
     });
     this.selection1 = new SelectionModel<Element>(true, []);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected2() {
    const numSelected = this.selection2.selected.length;
    const numRows = this.dataSource2.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle2() {
    this.isAllSelected2() ?
        this.selection2.clear() :
        this.dataSource2.data.forEach(row => this.selection2.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel2(row?: Element): string {
    if (!row) {
      return `${this.isAllSelected2() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection2.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  onDone2() {
    this.selection2.selected.forEach(item => {
       let index: number = this.data2.findIndex(d => d === item);
       console.log(this.data2.findIndex(d => d === item));
       this.data2.splice(index, 1)
       this.dataSource2 = new MatTableDataSource<Element>(this.data2);
     });
     this.selection2 = new SelectionModel<Element>(true, []);
  }

}
