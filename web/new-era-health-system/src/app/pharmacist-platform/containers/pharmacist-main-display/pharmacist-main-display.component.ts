import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pharmacist-main-display',
  template: `
    <div class="main-layout" fxLayoutAlign="center center" fxLayout="row">
      <img src="../../../../assets/pharmacy-logo.png"/>
    </div>
  `,
  styleUrls: ['./pharmacist-main-display.component.scss']
})
export class PharmacistMainDisplayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
