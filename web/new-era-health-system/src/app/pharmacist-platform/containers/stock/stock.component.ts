import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

export interface Medicine {
    name: string;
    message: string;
  }

@Component({
  selector: 'app-stock',
  template: `
    <div class="main-layout">
      
      <div class="search">
        <textarea [matAutosizeMinRows]="1" matInput matTextareaAutosize [(ngModel)] = "str" placeholder="Pick a medicine"></textarea> 
        <button mat-raised-button color="primary" (click)='onCheck(lblName.id)'>Check</button>
      </div>
      <div class="result">
      <ng-container>
      <p>
          <label #lblName id="lblName">
  
          </label>
      </p>
      </ng-container>
      </div>
    </div>
  `,
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

    str:string;

    myControl = new FormControl();
    options: Medicine[] = [
      {name: 'Paracetamol', message: '500 on stock'},
      {name: 'Algolcalmin', message: '2000 on stock'},
      {name: 'Colebil', message: 'None on stock. Try Triferment as alternative. Pharmacy LifeHealth has 120 on stock.'},
      {name: 'Triferment', message: '150 on stock'}
    ];
    
    ngOnInit() {
     
    }

    onCheck(id) {
        let val ='';
        this.options.forEach(option =>{ if(this.str === option.name) val = option.message})
        if(val === '') {
            val = "We have no record of " + this.str + " medicine";
        }
        document.getElementById(id).innerHTML = val;
    }
  
  }