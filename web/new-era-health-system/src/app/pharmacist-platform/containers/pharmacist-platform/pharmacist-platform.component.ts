import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pharmacist-platform',
  template: `
  <div class="main-layout" fxLayout="row">
  <div fxFlex="15%">
    <app-pharmacist-sidenav></app-pharmacist-sidenav>
  </div>
  <div fxFlex="100%" fxLayout="column">
    <div>
      <app-pharmacist-topbar></app-pharmacist-topbar>
    </div>
    <div class="main-layout">
      <router-outlet></router-outlet>
    </div>
  </div>
</div>
  `,
  styleUrls: ['./pharmacist-platform.component.scss']
})
export class PharmacistPlatformComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
