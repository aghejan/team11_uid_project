import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {TableColumns} from "../../types/table-columns.class";

@Component({
  selector: 'app-data-table',
  template: `
    <mat-card fxLayout="column" style="margin-bottom: 16px">
      <div class="container full-height" fxLayout="column" fxFlexFill>
        <div *ngIf="title || titleIcon" fxLayout="row" fxLayoutAlign="space-between center" class="mat-title">
          <div fxLayout="row" fxLayoutAlign="start center" fxLayoutGap="8px" *ngIf="title">
            <mat-icon *ngIf="titleIcon">{{ titleIcon }}</mat-icon>
            <div class="mat-title no-margin">
              {{ title | titlecase }}
            </div>
          </div>
          <div fxLayout="row" fxLayoutGap="8px">
            <button mat-flat-button color="accent" *ngIf="canAdd" (click)="add.emit()">ADD</button>
          </div>
        </div>
        <mat-form-field appearance="standard"
                        *ngIf="enableFiltering && dataSource && dataSource.data.length !== 0"
                        class="full-width"
        >
          <mat-label>{{filterLabel}}</mat-label>
          <input matInput (keyup)="applyFilter($event.target.value)" />
        </mat-form-field>
        <div
          style="position: relative"
          fxLayout="row"
        >
          <table mat-table [dataSource]="dataSource" matSort class="full-width">
            <ng-container
              *ngFor="let column of (tableColumns | keyvalue)"
              matColumnDef="{{ column.key }}"
            >
              <ng-container *ngIf="column.key === 'actions'">
                <th mat-header-cell *matHeaderCellDef ><div class="action-column">Actions</div></th>
                <td mat-cell *matCellDef="let element" class="action-column">
                  <button
                    mat-icon-button
                    [matMenuTriggerFor]="menu"
                    (click)="activeElement.emit(element); $event.stopPropagation()"
                  >
                    <mat-icon>more_vert</mat-icon>
                  </button>
                  <mat-menu #menu="matMenu">
                    <ng-content></ng-content>
                    <button
                      *ngIf="canEdit"
                      mat-menu-item
                      (click)="edit.emit(element)"
                    >
                      <mat-icon matPrefix>edit</mat-icon>
                      Edit
                    </button>
                    <button
                      *ngIf="canRemove"
                      mat-menu-item
                      (click)="remove.emit(element)"
                    >
                      <mat-icon
                        matPrefix
                        color="warn"
                      >delete</mat-icon>
                      Remove
                    </button>
                  </mat-menu>
                </td>
              </ng-container>
              <ng-container *ngIf="column.key !== 'actions'">
                <th mat-header-cell *matHeaderCellDef>
                  {{ column.value.title | titlecase }}
                </th>
                <td mat-cell *matCellDef="let element">
                  <div *ngIf="!column.value.isArray; else isArray">
                    <div *ngIf="column.value.displayFn; else noProperty">
                      {{
                      column.value.displayFn(element)
                      }}
                    </div>
                    <ng-template #noProperty>
                      {{
                      element[column.key]
                        ? element[column.key]
                        : '-'
                      }}</ng-template
                    >
                  </div>
                  <ng-template #isArray>
                    <div *ngFor="let item of element[column.key]">
                      <div *ngIf="column.value.displayFn; else noPropertyArray">
                        {{ item ? column.value.displayFn(item) : '-' }}
                      </div>
                      <ng-template #noPropertyArray>
                        {{ item ? item : '-' }}</ng-template
                      >
                    </div>
                    <div *ngIf="element[column.key].length === 0">-</div>
                  </ng-template>
                </td>
              </ng-container>
            </ng-container>
            <tr
              mat-header-row
              *matHeaderRowDef="displayedColumns; sticky: true"
            ></tr>
            <tr
              [ngClass]="{ selectable: selectable }"
              (click)="onSelect(row)"
              mat-row
              *matRowDef="let row; columns: displayedColumns"
            ></tr>
          </table>
        </div>
        <div
          [hidden]="
            !(
              dataSource &&
              enablePagination &&
              !loading &&
              dataSource.filteredData.length !== 0
            )
          "
        >
          <mat-paginator [pageSizeOptions]="[10, 20, 30]"></mat-paginator>
        </div>
        <div
          *ngIf="
            (!dataSource ||
              dataSource.filteredData.length === 0 ||
              dataSource.data.length === 0) &&
            !loading
          "
        >
          <h4 class="text-center">
            There are no {{ title | lowercase }}
          </h4>
        </div>
        <div *ngIf="loading">
          <mat-spinner color="primary" style="margin: auto"></mat-spinner>
        </div>
      </div>
    </mat-card>
  `,
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() title: string;
  @Input() titleIcon: string;
  @Input() loading: boolean;
  @Input() enablePagination = true;
  @Input() enableFiltering = false;
  @Input() tableColumns: TableColumns;
  @Input() canAdd = true;
  @Input() tableHeight: number;
  @Input() canEdit = true;
  @Input() canRemove = true;
  @Input() selectable = false;
  @Input() filterLabel = 'Filter';

  @Input() set data(data: any[]) {
    if (data) {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      if (this.enablePagination) {
        this.dataSource.paginator = this.paginator;
      }
    }
  }

  displayedColumns: string[];
  dataSource: MatTableDataSource<any>;

  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() remove: EventEmitter<any> = new EventEmitter();
  @Output() add: EventEmitter<void> = new EventEmitter();
  @Output() select: EventEmitter<any> = new EventEmitter();
  @Output() activeElement: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor() {}

  ngOnInit() {
    this.displayedColumns = Object.keys(this.tableColumns);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onSelect(row) {
    if (this.selectable) {
      this.select.emit(row);
    }
  }
}
