import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  template: `
    <div class="middle">
      <mat-icon>check_circle_outline</mat-icon>
    </div>
  `,
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
