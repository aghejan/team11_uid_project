import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-authentication-modal',
  template: `
    <div class="full-height" fxLayout="column">
      <div fxLayout="row" fxLayoutAlign="space-between start">
        <div class="mat-title no-margin">New Era Health System</div>
        <button mat-icon-button>
          <mat-icon>close</mat-icon>
        </button>
      </div>
      <div class="full-height" fxLayout="column" fxLayoutAlign="center center">
        <webcam [width]="450" [height]="400"></webcam>
      </div>
    </div>
  `,
  styleUrls: ['./authentication-modal.component.scss']
})
export class AuthenticationModalComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<AuthenticationModalComponent>
  ) { }

  ngOnInit() {
    setTimeout(() => this.dialogRef.close(), 3000);
  }

}
