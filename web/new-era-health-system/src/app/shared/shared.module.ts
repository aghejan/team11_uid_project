import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DataTableComponent} from "./components/data-table/data-table.component";
import {MaterialModule} from "../material/material.module";
import {AuthenticationModalComponent} from './components/authentication-modal/authentication-modal.component';
import {WebcamModule} from "ngx-webcam";
import { ConfirmComponent } from './components/confirm/confirm.component';


@NgModule({
  declarations: [DataTableComponent, AuthenticationModalComponent, ConfirmComponent, ConfirmComponent],
  imports: [
    CommonModule,
    MaterialModule,
    WebcamModule
  ],
  exports: [DataTableComponent, MaterialModule, AuthenticationModalComponent, ConfirmComponent],
  entryComponents: [AuthenticationModalComponent]
})
export class SharedModule {
}
