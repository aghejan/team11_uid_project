export class TableColumns {
  [name: string]: {
    title: string;
    isArray?: boolean;
    displayFn?: (value: any) => string;
    translate?: boolean;
  };
}
