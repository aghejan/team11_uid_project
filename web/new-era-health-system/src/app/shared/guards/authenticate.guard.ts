import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {AuthenticationModalComponent} from '../components/authentication-modal/authentication-modal.component';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateGuard implements CanActivate {

  constructor(
    private dialog: MatDialog
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.dialog.open(AuthenticationModalComponent, {width: '500px', autoFocus: false, height: '450px'})
      .afterClosed()
      .pipe(
        map(() => true)
      );
  }

}
