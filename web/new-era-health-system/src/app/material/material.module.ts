import {NgModule} from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout';

/* Breakpoints based on bootstrap */

const CUSTOM_BREAKPOINTS = [
  { alias: 'xs', mediaQuery: 'only screen and (min-width: 0px)' },
  { alias: 'sm', mediaQuery: 'only screen and (min-width: 576px)' },
  { alias: 'md', mediaQuery: 'only screen and (min-width: 768px)' },
  { alias: 'lg', mediaQuery: 'only screen and (min-width: 992px)' },
  { alias: 'xl', mediaQuery: 'only screen and (min-width: 1200px)' }
];

import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatToolbarModule,
  MatTooltipModule,
  MatMenuModule,
  MatSnackBarModule,
  MatChipsModule,
  MatSelectModule,
  MatRadioModule,
  MatTabsModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatAutocompleteModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatStepperModule,
  MatExpansionModule
} from '@angular/material';

@NgModule({
  imports: [
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatSnackBarModule,
    MatChipsModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    FlexLayoutModule.withConfig({ disableDefaultBps: true }, CUSTOM_BREAKPOINTS)
  ],
  exports: [
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatSnackBarModule,
    MatChipsModule,
    MatSelectModule,
    MatRadioModule,
    MatTabsModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    FlexLayoutModule
  ]
})
export class MaterialModule {}
