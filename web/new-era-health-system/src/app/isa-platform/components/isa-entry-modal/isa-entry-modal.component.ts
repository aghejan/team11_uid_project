import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-isa-entry-modal',
  template: `
    <p>
      isa-entry-modal works!
    </p>
  `,
  styleUrls: ['./isa-entry-modal.component.scss']
})
export class IsaEntryModalComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<IsaEntryModalComponent>
  ) {
  }

  ngOnInit() {
  }

}
