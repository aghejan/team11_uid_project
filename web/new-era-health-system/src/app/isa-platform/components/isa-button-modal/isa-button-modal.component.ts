import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-isa-button-modal',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div *ngIf="data; else noData">
        <div class="mat-title">{{data}}</div>
        <ul>
          <li [style.color]="color">Chickenpox vaccine</li>
          <li [style.color]="color">Pneumococcal vaccine</li>
        </ul>
      </div>
      <ng-template #noData>
        <div fxLayout="column" fxLayoutAlign="center center" fxLayoutGap="8px">
          <div>No history regarding any</div>
          <div>chronic diseases</div>
        </div>
      </ng-template>
      <div fxLayoutAlign="end center">
        <button mat-raised-button (click)="onOK()">
          OK
        </button>
      </div>
    </div>
  `,
  styleUrls: ['./isa-button-modal.component.scss']
})
export class IsaButtonModalComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<IsaButtonModalComponent>
  ) {
  }

  ngOnInit() {
  }

  onOK() {
    this.dialogRef.close();
  }

}
