import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-isa-sidenav',
  template: `
  <div class="full-width border" fxLayout="column" fxLayoutAlign="start center">
    <mat-list>
    <mat-list-item><img src="../../../../assets/mainLogo.png" height="40px"/>New Era Health System</mat-list-item>
    </mat-list>
    <mat-divider class="full-width"></mat-divider>
    <mat-nav-list class="full-width items" fxLayout="column" fxLayoutAlign="start center">
      <mat-list-item (click)="onGoToMedicalHistory()">Medical History</mat-list-item>
      <mat-divider class="full-width"></mat-divider>
      <mat-list-item (click)="onGoToMedicalExemptions()">Medical Exemptions</mat-list-item>
      <mat-divider class="full-width"></mat-divider>
    </mat-nav-list>
    <div *ngIf="checkPageUrl()" class="full-width full-height" fxLayout="column" fxLayoutAlign="end center">
      <mat-divider class="full-width"></mat-divider>
      <mat-list class="full-width">
        <mat-list-item fxLayout="row">
          <div fxLayoutAlign="start center">
            Bianca Irimescu
          </div>
          <mat-icon class="cursor-pointer" fxLayoutAlign="end center" (click)="onCloseMedicalHistory()">close</mat-icon>
        </mat-list-item>
      </mat-list>
      <mat-divider class="full-width"></mat-divider>
      <img class="full-width" src="../../../../assets/isa-female-avatar.jpg"/>
    </div>
  </div>
  `,
  styleUrls: ['./isa-sidenav.component.scss']
})
export class IsaSidenavComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  onGoToMedicalHistory() {
    this.router.navigate(['medical-history', 4], {relativeTo: this.route});
  }

  onGoToMedicalExemptions() {
    this.router.navigate(['medical-exemptions'], {relativeTo: this.route});
  }

  checkPageUrl() {
    return this.router.url.split('/')[2] === 'medical-history';
  }

  onCloseMedicalHistory() {
    this.router.navigate(['isa-platform']);
  }

}
