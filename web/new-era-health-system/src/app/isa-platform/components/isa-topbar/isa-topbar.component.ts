import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-isa-topbar',
  template: `
  <div class="main-layout">
  <div class="full-width padding-right-8" fxLayout="row" fxLayoutAlign="end center" fxLayoutGap="8px">
    <div class="mat-title no-margin">
      Welcome, Administrator
    </div>
    <mat-icon>account_circle</mat-icon>
  </div>

</div>
  `,
  styleUrls: ['./isa-topbar.component.scss']
})
export class IsaTopbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
