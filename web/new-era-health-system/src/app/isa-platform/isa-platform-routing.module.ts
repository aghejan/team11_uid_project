import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsaPlatformComponent } from './containers/isa-platform/isa-platform.component';
import { IsaMainDisplayComponent } from './containers/isa-main-display/isa-main-display.component';
import { IsaMedicalExemptionsComponent } from './containers/isa-medical-exemptions/isa-medical-exemptions.component';
import { IsaMedicalHistoryComponent } from './containers/isa-medical-history/isa-medical-history.component';
import { AuthenticateGuard } from '../shared/guards/authenticate.guard';


const routes: Routes = [
  {
    path: '',
    component: IsaPlatformComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: IsaMainDisplayComponent
      },
      {
        path: 'medical-history/:id',
        pathMatch: 'full',
        canActivate: [AuthenticateGuard],
        component: IsaMedicalHistoryComponent
      },
      {
        path: 'medical-exemptions',
        pathMatch: 'full',
        component: IsaMedicalExemptionsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IsaPlatformRoutingModule { }
