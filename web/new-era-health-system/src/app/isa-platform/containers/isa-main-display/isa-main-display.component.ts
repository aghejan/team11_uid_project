import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-isa-main-display',
  template: `
  <div class="main-layout" fxLayoutAlign="center center" fxLayout="row">
    <img src="../../../../assets/isa-logo.png"/>
  </div>
  `,
  styleUrls: ['./isa-main-display.component.scss']
})
export class IsaMainDisplayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
