import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../format-datepicker';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-isa-medical-exemptions',
  template: `
    <div class="main-layout" fxLayout="column" fxLayoutAlign="start start" fxLayoutGap="32px">
      <div class="mat-title no-margin">
        Medical Exemptions
      </div>
      <div fxLayout="row" fxLayoutAlign="center center" fxLayoutGap="128px">
        <mat-form-field class="full-width">
          <mat-label>Start Date</mat-label>
          <input matInput [matDatepicker]="startPicker" (dateChange)="onChangeStartDate()" [formControl]="startDateControl">
          <mat-datepicker-toggle matSuffix [for]="startPicker"></mat-datepicker-toggle>
          <mat-datepicker #startPicker disabled="false"></mat-datepicker>
          <mat-error *ngIf="startDateControl.hasError('required')">Start Date is required</mat-error>
        </mat-form-field>
        <mat-form-field class="full-width">
          <mat-label>End Date</mat-label>
          <input matInput [min]="startDateControl.value"
            [matDatepicker]="endPicker"
            (dateChange)="onChangeEndDate()"
            [formControl]="endDateControl"
            >
          <mat-datepicker-toggle matSuffix [for]="endPicker"></mat-datepicker-toggle>
          <mat-datepicker #endPicker disabled="{{!startDateControl.value}}"></mat-datepicker>
          <mat-error *ngIf="endDateControl.hasError('required')">End Date is required</mat-error>
        </mat-form-field>
        <button mat-raised-button class="full-width" (click)="onClearDates()">Clear dates</button>
      </div>
      <app-data-table
        class="full-width"
        [title]="'Employees Medical Exemptions'"
        [enableFiltering]="false"
        [tableColumns]="tableColumns"
        [data]="filteredExemptions$ | async"
        [selectable]="true"
        [canAdd]="false"
        [canEdit]="false"
        [canRemove]="false"
        (select)="onSelect($event)"
        >
      </app-data-table>
    </div>
  `,
  styleUrls: ['./isa-medical-exemptions.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})

export class IsaMedicalExemptionsComponent implements OnInit {
  startDateControl = new FormControl({value: moment(), disabled: true}, Validators.required);
  endDateControl = new FormControl({value: moment(), disabled: true}, Validators.required);
  disabledEnd = false;
  startReference: string[];
  endReference: string[];
  filteredExemptions$: BehaviorSubject<any> = new BehaviorSubject(null);

  exemptions = [
    {
      name: 'Bianca Irimescu',
      startDate: '10-01-2020',
      endDate: '16-01-2020'
    },
    {
      name: 'Vlad Pop',
      startDate: '11-11-2019',
      endDate: '25-11-2019'
    },
    {
      name: 'Ramona Berar',
      startDate: '11-11-2019',
      endDate: '13-11-2019'
    },
    {
      name: 'Patricia Dragota',
      startDate: '04-11-2019',
      endDate: '08-11-2019'
    },
    {
      name: 'Cosmin Onaca',
      startDate: '04-11-2019',
      endDate: '06-11-2019'
    },
    {
      name: 'Bogdan Rusu',
      startDate: '01-11-2019',
      endDate: '07-11-2019'
    }
  ];

  tableColumns = {
    name: {
      title: 'Name'
    },
    startDate: {
      title: 'Starting from'
    },
    endDate: {
      title: 'Until'
    }
  };

  constructor() { }

  ngOnInit() {
    this.filteredExemptions$.next(this.exemptions);
  }

  onSelect(exemption: any) {

  }

  onChangeStartDate() {
    this.filteredExemptions$.next(this.exemptions);
    this.endDateControl.setValue(moment());
  }

  onChangeEndDate() {
    this.filteredExemptions$.next(this.exemptions);
    this.filterExemptions();
  }

  filterExemptions() {
    this.startReference = moment(this.startDateControl.value).format('DD-MM-YYYY').split('-');
    this.endReference = moment(this.endDateControl.value).format('DD-MM-YYYY').split('-');

    this.filteredExemptions$.next(this.exemptions.filter((exemption) =>
      this.checkStartDate(exemption.startDate.split('-'))
      && this.checkEndDate(exemption.endDate.split('-'))
    ));
  }

  checkStartDate(dateReference: string[]): boolean {
    const startCheck = +this.startReference[2] <= +dateReference[2]
      ? +this.startReference[0] <= +dateReference[0]
        ? +this.startReference[1] <= +dateReference[1] ? true : false
        : false
      : false;
    console.log(this.startReference);
    console.log(dateReference);
    console.log(startCheck);
    return startCheck;
  }

  checkEndDate(dateReference: string[]): boolean {
    const endCheck = +this.endReference[2] >= +dateReference[2]
      ? +this.endReference[0] >= +dateReference[0]
        ? +this.endReference[1] >= +dateReference[1] ? true : false
        : false
      : false;
    console.log(this.endReference);
    console.log(dateReference);
    console.log(endCheck);
    return endCheck;
  }

  onClearDates() {
    this.filteredExemptions$.next(this.exemptions);
    this.startDateControl.setValue(moment());
    this.endDateControl.setValue((moment));
  }
}
