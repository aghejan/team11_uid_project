import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IsaButtonModalComponent } from '../../components/isa-button-modal/isa-button-modal.component';
import { IsaEntryModalComponent } from '../../components/isa-entry-modal/isa-entry-modal.component';

@Component({
  selector: 'app-isa-medical-history',
  template: `
    <div class="main-layout" fxLayout="column" fxLayoutAlign="start start" fxLayoutGap="32px">
      <div fxLayout="row" fxLayoutAlign="start center" fxLayoutGap="8px">
        <mat-icon>person</mat-icon>
        <div class="mat-title no-margin">
          Bianca Irimescu
        </div>
      </div>
      <div class="full-width" fxLayout="row" fxLayoutAlign="center center" fxLayoutGap="128px">
        <button mat-raised-button (click)="onVaccineStatus()">Vaccine Status</button>
        <button mat-raised-button (click)="onCronicDiseases()">Cronic Diseases</button>
      </div>
      <app-data-table
        class="full-width"
        [title]="'Latest Medical Checkoups and Surgeries'"
        [enableFiltering]="false"
        [tableColumns]="tableColumns"
        [data]="medicalEntries"
        [selectable]="true"
        [canAdd]="false"
        [canEdit]="false"
        [canRemove]="false"
        (select)="onSelect($event)"
        >
      </app-data-table>
    </div>
  `,
  styleUrls: ['./isa-medical-history.component.scss']
})
export class IsaMedicalHistoryComponent implements OnInit {

  medicalEntries = [
    {
      name: 'Dental Surgery',
      date: '10-11-2019'
    },
    {
      name: 'Orthopedic Checkup',
      date: '29-07-2019'
    },
    {
      name: 'Pulmonar Checkup',
      date: '01-12-2018'
    }
  ];

  tableColumns = {
      name: {
        title: 'Name'
      },
      date: {
        title: 'Date'
      }
    };

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  onVaccineStatus() {
    this.dialog.open(IsaButtonModalComponent, {autoFocus: false, minWidth: '350px', data: 'Vaccines taken'})
      .afterClosed()
      .subscribe();
  }

  onCronicDiseases() {
    this.dialog.open(IsaButtonModalComponent, {autoFocus: false, minWidth: '350px'})
      .afterClosed()
      .subscribe();
  }

  onSelect(medicalEntry: any) {
    // this.dialog.open(IsaEntryModalComponent, {autoFocus: false, minWidth: '650px', data: 'entry'})
    //   .afterClosed()
    //   .subscribe();
  }

}
