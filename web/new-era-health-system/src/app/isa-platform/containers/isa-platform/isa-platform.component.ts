import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-isa-platform',
  template: `
    <div class="main-layout" fxLayout="row">
      <div fxFlex="15%">
        <app-isa-sidenav></app-isa-sidenav>
      </div>
      <div fxFlex="100%" fxLayout="column">
        <div>
          <app-isa-topbar></app-isa-topbar>
        </div>
        <div class="main-layout">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./isa-platform.component.scss']
})
export class IsaPlatformComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
