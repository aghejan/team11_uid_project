import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IsaPlatformRoutingModule } from './isa-platform-routing.module';
import { IsaPlatformComponent } from './containers/isa-platform/isa-platform.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IsaTopbarComponent } from './components/isa-topbar/isa-topbar.component';
import { IsaSidenavComponent } from './components/isa-sidenav/isa-sidenav.component';
import { IsaMainDisplayComponent } from './containers/isa-main-display/isa-main-display.component';
import { IsaMedicalHistoryComponent } from './containers/isa-medical-history/isa-medical-history.component';
import { IsaMedicalExemptionsComponent } from './containers/isa-medical-exemptions/isa-medical-exemptions.component';
import { SharedModule } from '../shared/shared.module';
import { AuthenticationModalComponent } from '../shared/components/authentication-modal/authentication-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { IsaButtonModalComponent } from './components/isa-button-modal/isa-button-modal.component';
import { IsaEntryModalComponent } from './components/isa-entry-modal/isa-entry-modal.component';

@NgModule({
  declarations: [
    IsaPlatformComponent,
    IsaTopbarComponent,
    IsaSidenavComponent,
    IsaMainDisplayComponent,
    IsaMedicalHistoryComponent,
    IsaMedicalExemptionsComponent,
    IsaButtonModalComponent,
    IsaEntryModalComponent
  ],
  imports: [
    CommonModule,
    IsaPlatformRoutingModule,
    FlexLayoutModule,
    SharedModule,
    ReactiveFormsModule
  ],
  entryComponents: [AuthenticationModalComponent, IsaButtonModalComponent, IsaEntryModalComponent]
})
export class IsaPlatformModule { }
