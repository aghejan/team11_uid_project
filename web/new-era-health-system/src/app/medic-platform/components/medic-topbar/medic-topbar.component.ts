import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medic-topbar',
  template: `
    <div class="main-layout">
      <div class="full-width padding-right-8" fxLayout="row" fxLayoutAlign="end center" fxLayoutGap="8px">
        <div class="mat-title no-margin">
          Welcome, Doctor
        </div>
        <mat-icon>account_circle</mat-icon>
      </div>

    </div>
  `,
  styleUrls: ['./medic-topbar.component.scss']
})
export class MedicTopbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
