import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-medic-sidenav',
  template: `
    <div class="full-width border" fxLayout="column" fxLayoutAlign="start center">
      <mat-list>
        <mat-list-item><img src="../../../../assets/mainLogo.png" height="40px"/>New Era Health System</mat-list-item>
      </mat-list>
      <mat-divider class="full-width"></mat-divider>
      <mat-nav-list class="full-width items" fxLayout="column" fxLayoutAlign="start center">
        <mat-list-item (click)="onGoToPatients()">Patients</mat-list-item>
        <mat-divider class="full-width"></mat-divider>
        <mat-list-item (click)="onGoToAppointments()">Appointments</mat-list-item>
        <mat-divider class="full-width"></mat-divider>
      </mat-nav-list>
      <div *ngIf="checkPageUrl()" class="full-width full-height" fxLayout="column" fxLayoutAlign="end center">
      <mat-divider class="full-width"></mat-divider>
      <mat-list class="full-width">
        <mat-list-item fxLayout="row">
          <div fxLayoutAlign="start center">
            Tony Stark
          </div>
          <mat-icon class="cursor-pointer" fxLayoutAlign="end center" (click)="onCloseMedicationPlans()">close</mat-icon>
        </mat-list-item>
      </mat-list>
      <mat-divider class="full-width"></mat-divider>
      <img class="full-width" src="../../../../assets/tony.png"/>
    </div>
    </div>

  `,
  styleUrls: ['./medic-sidenav.component.scss']
})
export class MedicSidenavComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  onGoToPatients() {
    this.router.navigate(['patients'], {relativeTo: this.route});
  }

  onGoToAppointments() {
    this.router.navigate(['appointments'], {relativeTo: this.route});

  }

  checkPageUrl() {
    return this.router.url.split('/')[2] === 'patients' && this.router.url.split('/')[3];
  }

}
