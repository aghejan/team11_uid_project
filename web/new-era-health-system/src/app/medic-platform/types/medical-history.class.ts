class MedicalHistory {
  id: number;
  diagnostic: string;
  consultationDate: string;
}
