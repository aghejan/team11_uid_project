export class Patient {
  id: number;
  name: string;
  dateOfBirth: string;
  email: string;
  sex: string;
}
