import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-send-to-doctor-modal',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div class="mat-title">Send {{data.name}} to a specialised doctor</div>
      <div fxLayout="column" fxLayoutGap="8px">
        <mat-form-field class="full-width">
          <mat-label>Reason</mat-label>
          <textarea [matAutosizeMinRows]="5" matInput matTextareaAutosize [formControl]="reasonControl"></textarea>
          <mat-error *ngIf="reasonControl.hasError('required')">Reason is required</mat-error>
        </mat-form-field>
      </div>
      <mat-form-field class="full-width">
        <mat-label>Doctor speciality</mat-label>
        <mat-select [formControl]="doctorSpecialisationControl">
          <mat-option *ngFor="let item of specialisations" [value]="item">{{item}}</mat-option>
        </mat-select>
        <mat-error *ngIf="doctorSpecialisationControl.hasError('required')">Doctor Speciality is required</mat-error>
      </mat-form-field>
      <mat-form-field class="full-width">
        <mat-label>Doctor</mat-label>
        <mat-select [formControl]="doctorControl" [disabled]="doctorSpecialisationControl.invalid">
          <mat-option *ngFor="let doctor of doctors" [value]="doctor">{{doctor}}</mat-option>
        </mat-select>
        <mat-error *ngIf="doctorControl.hasError('required')">Doctor is required</mat-error>
      </mat-form-field>
    </div>
    <button mat-raised-button color="primary" (click)="onSend()" [disabled]="form.invalid">
      SEND
    </button>
  `,
  styleUrls: ['./send-to-doctor-modal.component.scss']
})
export class SendToDoctorModalComponent implements OnInit {
  form: FormGroup;
  specialisations: string[] = ['Cardiology', 'Anesthesiology', 'Dermatology', 'Neurology', 'Pathology'];
  doctors: string[] = ['Ciprian Maris', 'Fechete Paula', 'Maier Patricia', 'Bogdan Lucian'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<SendToDoctorModalComponent>,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      reason: ['', Validators.required],
      doctorSpecialisation: ['', Validators.required],
      doctor: ['', Validators.required]
    });
  }

  get reasonControl() {
    return this.form.get('reason');
  }

  get doctorSpecialisationControl() {
    return this.form.get('doctorSpecialisation');
  }

  get doctorControl() {
    return this.form.get('doctor');
  }

  onSend() {
    this.dialogRef.close(this.doctorControl.value);
  }
}
