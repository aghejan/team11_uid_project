import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Medication} from "../../types/medication.class";

@Component({
  selector: 'app-create-recipe-modal',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div class="mat-title">Create Recipe</div>
      <div fxLayout="column" fxLayoutGap="8px">
        <div fxLayout="row" fxLayoutGap="16px">
          <mat-form-field class="full-width">
            <mat-label>Consultation Date</mat-label>
            <input matInput [matDatepicker]="picker" [formControl]="consultationDateControl">
            <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
            <mat-datepicker #picker></mat-datepicker>
            <mat-error *ngIf="consultationDateControl.hasError('required')">Consultation date is required</mat-error>
          </mat-form-field>
          <mat-form-field class="full-width">
            <mat-label>Diagnostic</mat-label>
            <input matInput [formControl]="diagnosticControl">
            <mat-error *ngIf="diagnosticControl.hasError('required')">Diagnostic is required</mat-error>
          </mat-form-field>
        </div>
        <div class="mat-subheading-2">Treatment Period</div>
        <div fxLayout="row" fxLayoutGap="16px">
          <mat-form-field class="full-width">
            <mat-label>Start Date</mat-label>
            <input matInput [matDatepicker]="startPicker" [formControl]="startDateControl">
            <mat-datepicker-toggle matSuffix [for]="startPicker"></mat-datepicker-toggle>
            <mat-datepicker #startPicker></mat-datepicker>
            <mat-error *ngIf="startDateControl.hasError('required')">Start date is required</mat-error>
          </mat-form-field>
          <mat-form-field class="full-width">
            <mat-label>End Date</mat-label>
            <input matInput [min]="startDateControl.value" [matDatepicker]="endPicker" [formControl]="endDateControl">
            <mat-datepicker-toggle matSuffix [for]="endPicker"></mat-datepicker-toggle>
            <mat-datepicker #endPicker></mat-datepicker>
            <mat-error *ngIf="endDateControl.hasError('required')">End date is required</mat-error>
          </mat-form-field>
        </div>
        <div class="mat-subheading-2">Medications</div>
        <div>
          <div fxLayout="row" fxLayoutAlign="start center" class="full-width"
               *ngFor="let medication of medicationsControl.controls; let i = index; let last = last">
            <div fxLayout="row" fxLayoutGap="16px" class="full-width">
              <mat-form-field class="full-width">
                <mat-label>Medication</mat-label>
                <mat-select [formControl]="getMedicationControl(i)">
                  <mat-option *ngFor="let item of medications" [value]="item">{{item.name}}</mat-option>
                </mat-select>
                <mat-error *ngIf="getMedicationControl(i).hasError('required')">Medication is required</mat-error>
              </mat-form-field>
              <mat-form-field class="full-width">
                <mat-label>Intake Interval</mat-label>
                <input matInput [formControl]="getIntakeInterval(i)">
                <mat-error *ngIf="getIntakeInterval(i).hasError('required')">Intake interval is required</mat-error>
              </mat-form-field>
            </div>
            <button *ngIf="last" mat-icon-button (click)="onAddMedication()">
              <mat-icon>add</mat-icon>
            </button>
            <button *ngIf="!last" mat-icon-button (click)="onRemoveMedication(i)">
              <mat-icon>remove</mat-icon>
            </button>
          </div>
        </div>
      </div>
    </div>
    <button color="primary" mat-raised-button (click)="onCreate()" [disabled]="form.invalid">
      CREATE
    </button>
  `,
  styleUrls: ['./create-recipe-modal.component.scss']
})
export class CreateRecipeModalComponent implements OnInit {
  form: FormGroup;
  medications: Medication[];

  constructor(
    private dialogRef: MatDialogRef<CreateRecipeModalComponent>,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      consultationDate: [new Date(), Validators.required],
      diagnostic: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      medications: this.formBuilder.array([
        this.formBuilder.group({
          medication: ['', Validators.required],
          intakeInterval: ['', Validators.required]
        })
      ])
    });
    this.medications = this.getMedications();
  }

  onAddMedication() {
    this.medicationsControl.push(
      this.formBuilder.group({
        medication: ['', Validators.required],
        intakeInterval: ['', Validators.required]
      })
    );
  }

  onRemoveMedication(i) {
    this.medicationsControl.removeAt(i);
  }

  onCreate() {
    this.dialogRef.close(this.form.value);
  }

  get consultationDateControl() {
    return this.form.get('consultationDate');
  }

  get diagnosticControl() {
    return this.form.get('diagnostic');
  }

  get startDateControl() {
    return this.form.get('startDate');
  }

  get endDateControl() {
    return this.form.get('endDate');
  }

  get medicationsControl() {
    return this.form.get('medications') as FormArray;
  }

  getMedicationControl(index) {
    return this.medicationsControl.at(index).get('medication');
  }

  getIntakeInterval(index) {
    return this.medicationsControl.at(index).get('intakeInterval');
  }

  private getMedications() {
    return [
      {
        id: 1,
        name: 'Nurofen'
      }, {
        id: 2,
        name: 'Stepsils'
      }, {
        id: 3,
        name: 'Paracetamol'
      }
    ];
  }
}
