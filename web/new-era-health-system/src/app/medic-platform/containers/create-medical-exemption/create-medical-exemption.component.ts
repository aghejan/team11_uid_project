import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-create-medical-exemption',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div class="mat-title">Create Medical Exemption for {{data.name}}</div>
      <div fxLayout="column" fxLayoutGap="8px">
        <mat-form-field class="full-width">
          <mat-label>Reason</mat-label>
          <textarea [matAutosizeMinRows]="5" matInput matTextareaAutosize [formControl]="reasonControl"></textarea>
          <mat-error *ngIf="reasonControl.hasError('required')">Reason is required</mat-error>
        </mat-form-field>
      </div>
      <div class="mat-subheading-2">Period</div>
      <div fxLayout="row" fxLayoutGap="16px">
        <mat-form-field class="full-width">
          <mat-label>From</mat-label>
          <input matInput [matDatepicker]="picker" [formControl]="fromControl">
          <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker #picker></mat-datepicker>
          <mat-error *ngIf="fromControl.hasError('required')">From date is required</mat-error>
        </mat-form-field>
        <mat-form-field class="full-width">
          <mat-label>To</mat-label>
          <input [min]="fromControl.value" matInput [matDatepicker]="toPicker" [formControl]="toControl">
          <mat-datepicker-toggle matSuffix [for]="toPicker"></mat-datepicker-toggle>
          <mat-datepicker #toPicker></mat-datepicker>
          <mat-error *ngIf="toControl.hasError('required')">To date is required</mat-error>
        </mat-form-field>
      </div>
      <mat-form-field class="full-width">
        <mat-label>Institution</mat-label>
        <mat-select [formControl]="institutionControl">
          <mat-option *ngFor="let item of institutions" [value]="item">{{item}}</mat-option>
        </mat-select>
        <mat-error *ngIf="institutionControl.hasError('required')">Institution is required</mat-error>
      </mat-form-field>
    </div>
    <button mat-raised-button color="primary" (click)="onSend()" [disabled]="form.invalid">
      SEND
    </button>
  `,
  styleUrls: ['./create-medical-exemption.component.scss']
})
export class CreateMedicalExemptionComponent implements OnInit {
  form: FormGroup;
  institutions = ['UMF Cluj', 'Technical University of Cluj Napoca', 'Babes Bolyai University', 'Tiberiu Popoviciu High School'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CreateMedicalExemptionComponent>,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      reason: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      institution: ['', Validators.required]
    });
  }

  get reasonControl() {
    return this.form.get('reason');
  }

  get fromControl() {
    return this.form.get('from');
  }

  get toControl() {
    return this.form.get('to');
  }

  get institutionControl() {
    return this.form.get('institution');
  }

  onSend() {
    this.dialogRef.close(this.institutionControl.value);
  }
}
