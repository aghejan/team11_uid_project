import {Component, OnInit} from '@angular/core';
import {TableColumns} from '../../../shared/types/table-columns.class';
import {MatDialog} from "@angular/material/dialog";
import {CreateRecipeModalComponent} from "../create-recipe-modal/create-recipe-modal.component";

@Component({
  selector: 'app-patient-page',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div class="mat-title">Dragos Marian</div>
      <mat-card>
        <div fxLayout="row">
          <div fxFlex="33%" class="mat-subheading-2">Date of birth</div>
          <div fxFlex="33%" class="mat-subheading-2">Phone Number</div>
          <div fxFlex="33%" class="mat-subheading-2">Sex</div>
        </div>
        <div fxLayout="row">
          <div fxFlex="33%" class="mat-subheading-1 no-margin">22.08.1990</div>
          <div fxFlex="33%" class="mat-subheading-1 no-margin">0767625156</div>
          <div fxFlex="33%" class="mat-subheading-1 no-margin">M</div>
        </div>
      </mat-card>
      <div class="full-width" fxLayout="column" fxLayoutGap="16px">
        <div fxLayoutAlign="space-between start">
          <div class="mat-title no-margin">Medical History</div>
          <button mat-flat-button color="primary" (click)="onCreateRecipe()">
            CREATE RECIPE
          </button>
        </div>
        <app-data-table
          [enableFiltering]="false"
          [tableColumns]="tableColumns"
          [data]="history"
          [selectable]="false"
          [canAdd]="false"
        >
        </app-data-table>
      </div>
    </div>
  `,
  styleUrls: ['./patient-page.component.scss']
})
export class PatientPageComponent implements OnInit {
  tableColumns: TableColumns;
  history: MedicalHistory[];

  constructor(
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.history = this.getHistory();
    this.tableColumns = this.getTableColumns();
  }

  onCreateRecipe() {
    this.dialog.open(CreateRecipeModalComponent, {autoFocus: false, minWidth: '650px'})
      .afterClosed()
      .subscribe((data) => {
        if (!data){
          return;
        }

        const date: Date = data.consultationDate;
        this.history = [...this.history, {
          id: 5,
          diagnostic: data.diagnostic,
          consultationDate: `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
        }];
      });
  }

  private getHistory(): MedicalHistory[] {
    return [
      {
        id: 1,
        diagnostic: 'Pneumonia',
        consultationDate: '18.08.2019 12:20'
      },
      {
        id: 2,
        diagnostic: 'Bronchitis',
        consultationDate: '13.04.2019 18:43'
      }
    ];
  }

  private getTableColumns() {
    return {
      diagnostic: {
        title: 'Diagnostic'
      },
      consultationDate: {
        title: 'Consultation Date'
      }
    };
  }

}
