import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medic-main-display',
  template: `
    <div class="main-layout" fxLayoutAlign="center center" fxLayout="row">
      <img src="../../../../assets/logo.png"/>
    </div>
  `,
  styleUrls: ['./medic-main-display.component.scss']
})
export class MedicMainDisplayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
