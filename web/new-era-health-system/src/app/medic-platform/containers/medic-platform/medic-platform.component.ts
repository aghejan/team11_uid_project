import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medic-platform',
  template: `
    <div class="main-layout" fxLayout="row">
      <div fxFlex="15%">
        <app-medic-sidenav></app-medic-sidenav>
      </div>
      <div fxFlex="100%" fxLayout="column">
        <div>
          <app-medic-topbar></app-medic-topbar>
        </div>
        <div class="main-layout content-layout">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./medic-platform.component.scss']
})
export class MedicPlatformComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
