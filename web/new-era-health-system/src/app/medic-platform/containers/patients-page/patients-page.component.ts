import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Patient} from '../../types/patient.class';
import {TableColumns} from '../../../shared/types/table-columns.class';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CreateMedicalExemptionComponent} from '../create-medical-exemption/create-medical-exemption.component';
import {SendToDoctorModalComponent} from '../send-to-doctor-modal/send-to-doctor-modal.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-patients-page',
  template: `
    <div class="full-width full-height">
      <!--      <div class="full-width" fxLayoutAlign="space-between center">-->
      <!--        <div class="mat-headline no-margin">Patients</div>-->
      <!--        <mat-form-field>-->
      <!--          <mat-label>Search by name</mat-label>-->
      <!--          <input matInput [formControl]="searchControl"/>-->
      <!--        </mat-form-field>-->
      <!--      </div>-->
      <div>
        <app-data-table
          [title]="'Patients'"
          [titleIcon]="'person'"
          [enableFiltering]="true"
          [tableColumns]="tableColumns"
          [data]="patients"
          [selectable]="true"
          [canAdd]="false"
          [canEdit]="false"
          [canRemove]="false"
          (select)="onSelect($event)"
          (activeElement)="patient = $event"
        >
          <button
            mat-menu-item
            (click)="onCreateMedicalExemption()"
          >
            Create Medical Exemption
          </button>
          <button
            mat-menu-item
            (click)="onSendToSpecialisedDoctor()"
          >
            Send To Specialised Doctor
          </button>
        </app-data-table>
      </div>
    </div>
  `,
  styleUrls: ['./patients-page.component.scss']
})
export class PatientsPageComponent implements OnInit {
  searchControl: FormControl = new FormControl();
  patients: Patient[];
  patient: Patient;
  tableColumns: TableColumns;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.patients = this.getPatients();
    this.tableColumns = this.getTableColumns();
  }

  onSelect(patient: Patient) {
    this.router.navigate([patient.id], {relativeTo: this.route});
  }

  onCreateMedicalExemption() {
    this.dialog.open(CreateMedicalExemptionComponent, {autoFocus: false, minWidth: '650px', data: this.patient})
      .afterClosed()
      .subscribe((institute) => {
        if (institute) {
          this.snackBar.open(`Exemption sent successfully to ${institute}`, null, {duration: 2000});
        }
      });
  }

  onSendToSpecialisedDoctor() {
    this.dialog.open(SendToDoctorModalComponent, {autoFocus: false, minWidth: '650px', data: this.patient})
      .afterClosed()
      .subscribe((doctor) => {
        if (doctor) {
          this.snackBar.open(`The informations were sent to ${doctor}`, null, {duration: 2000});
        }
      });
  }

  private getPatients(): Patient[] {
    return [
      {
        id: 1,
        name: 'Dragos Marian',
        email: 'dragosmarian@gmail.com',
        dateOfBirth: '22.08.1990',
        sex: 'M'
      },
      {
        id: 2,
        name: 'Bodea Marian',
        email: 'bodeamaria@gmail.com',
        dateOfBirth: '15.04.1884',
        sex: 'F'
      },
      {
        id: 3,
        name: 'Macarie Dragos',
        email: 'macariedragos@gmail.com',
        dateOfBirth: '12.04.1888',
        sex: 'M'
      }
    ];
  }

  private getTableColumns() {
    return {
      name: {
        title: 'Name'
      },
      email: {
        title: 'Email'
      },
      dateOfBirth: {
        title: 'Date of Birth',
      },
      sex: {
        title: 'Sex'
      },
      actions: null
    };
  }
}
