import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-appointment-modal',
  template: `
    <div fxLayout="column" fxLayoutGap="24px">
      <div class="mat-title">Schedule Appointment</div>
      <div fxLayout="column" fxLayoutGap="8px">
        <mat-form-field class="full-width">
          <mat-label>Patient</mat-label>
          <input matInput [formControl]="patientControl">
        </mat-form-field>
      </div>
      <mat-form-field class="full-width">
        <mat-label>Date</mat-label>
        <input matInput [min]="now" [matDatepicker]="picker" [formControl]="dateControl">
        <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
        <mat-datepicker #picker></mat-datepicker>
        <mat-error *ngIf="dateControl.hasError('required')">Date is required</mat-error>
      </mat-form-field>

      <button mat-raised-button color="primary" (click)="onConfirm()" [disabled]="dateControl.invalid">
        SEND
      </button>
  `,
  styleUrls: ['./appointment-modal.component.scss']
})
export class AppointmentModalComponent implements OnInit {
  dateControl = new FormControl('', Validators.required);
  patientControl = new FormControl('');
  now = new Date();
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<AppointmentModalComponent>
  ) {
  }

  ngOnInit() {
    this.patientControl.disable();
    this.patientControl.setValue(this.data.name);
  }

  onConfirm() {
    this.dialogRef.close(this.dateControl.value);
  }
}
