import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {AppointmentModalComponent} from "../appointment-modal/appointment-modal.component";

@Component({
  selector: 'app-appointments',
  template: `
    <app-data-table
      [title]="'Appointments'"
      [enableFiltering]="false"
      [tableColumns]="tableColumns"
      [data]="appointments"
      [selectable]="true"
      [canAdd]="false"
      [canEdit]="false"
      [canRemove]="false"
      (select)="onSelect($event)"
    >
  `,
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  appointments = [
    {
      name: 'Marian Dragos',
      state: 'pending',
      date: '-'
    },
    {
      name: 'Bogdan Francis',
      state: 'scheduled',
      date: '20.01.2020'
    },
    {
      name: 'Ioana Pavel',
      state: 'scheduled',
      date: '25.01.2020'
    }
  ];
  tableColumns;

  constructor(
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.tableColumns = this.getTableColumns();
  }

  onSelect(appointment) {
    if (appointment.state === 'pending') {
      this.dialog.open(AppointmentModalComponent, {autoFocus: false, minWidth: '650px', data: appointment})
        .afterClosed()
        .subscribe((date: Date) => {
          if (date) {
            this.appointments[0].state = "scheduled";
            this.appointments[0].date = `${date.getDate()}.0${date.getMonth() + 1}.${date.getFullYear()}`;
          }
        });
    }
  }

  private getTableColumns() {
    return {
      name: {
        title: 'Name'
      },
      state: {
        title: 'State'
      },
      date: {
        title: 'Date',
      }
    };
  }
}
