import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicPlatformRoutingModule } from './medic-platform-routing.module';
import { MedicPlatformComponent } from './containers/medic-platform/medic-platform.component';
import { MedicMainDisplayComponent } from './containers/medic-main-display/medic-main-display.component';
import { MedicTopbarComponent } from './components/medic-topbar/medic-topbar.component';
import { MedicSidenavComponent } from './components/medic-sidenav/medic-sidenav.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PatientsPageComponent } from './containers/patients-page/patients-page.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import { PatientPageComponent } from './containers/patient-page/patient-page.component';
import {AuthenticationModalComponent} from "../shared/components/authentication-modal/authentication-modal.component";
import { CreateRecipeModalComponent } from './containers/create-recipe-modal/create-recipe-modal.component';
import { CreateMedicalExemptionComponent } from './containers/create-medical-exemption/create-medical-exemption.component';
import { SendToDoctorModalComponent } from './containers/send-to-doctor-modal/send-to-doctor-modal.component';
import { AppointmentsComponent } from './containers/appointments/appointments.component';
import { AppointmentModalComponent } from './containers/appointment-modal/appointment-modal.component';

@NgModule({
  declarations: [MedicPlatformComponent, MedicMainDisplayComponent, MedicTopbarComponent, MedicSidenavComponent, PatientsPageComponent, PatientPageComponent, CreateRecipeModalComponent, CreateMedicalExemptionComponent, CreateMedicalExemptionComponent, SendToDoctorModalComponent, AppointmentsComponent, AppointmentModalComponent],
  imports: [
    CommonModule,
    MedicPlatformRoutingModule,
    FlexLayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents: [AppointmentModalComponent, AuthenticationModalComponent, CreateRecipeModalComponent, SendToDoctorModalComponent, CreateMedicalExemptionComponent]
})
export class MedicPlatformModule { }
