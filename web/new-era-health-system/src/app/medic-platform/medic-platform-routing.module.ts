import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MedicPlatformComponent} from './containers/medic-platform/medic-platform.component';
import { MedicMainDisplayComponent } from './containers/medic-main-display/medic-main-display.component';
import {PatientsPageComponent} from './containers/patients-page/patients-page.component';
import {PatientPageComponent} from "./containers/patient-page/patient-page.component";
import {AuthenticateGuard} from "../shared/guards/authenticate.guard";
import {AppointmentsComponent} from "./containers/appointments/appointments.component";

const routes: Routes = [
  {
    path: '',
    component: MedicPlatformComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MedicMainDisplayComponent
      },
      {
        path: 'patients',
        component: PatientsPageComponent
      },
      {
        path: 'patients/:id',
        component: PatientPageComponent,
        canActivate: [AuthenticateGuard]
      },
      {
        path: 'appointments',
        component: AppointmentsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicPlatformRoutingModule { }
