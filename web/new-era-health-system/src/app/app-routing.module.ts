import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'medic-platform',
    loadChildren: './medic-platform/medic-platform.module#MedicPlatformModule'
  },
  {
    path: 'pharmacist-platform',
    loadChildren: './pharmacist-platform/pharmacist-platform.module#PharmacistPlatformModule'
  },
  {
    path: 'isa-platform',
    loadChildren: './isa-platform/isa-platform.module#IsaPlatformModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
