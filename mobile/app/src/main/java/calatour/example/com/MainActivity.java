package calatour.example.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences prefs = getSharedPreferences("Login", MODE_PRIVATE);
        boolean isUserLogged = prefs.getBoolean("logged", false);
        if(!isUserLogged) {
            setContentView(R.layout.activity_main);
        } else {
            String username = prefs.getString("username", "");
            String password = prefs.getString("password", "");
            Intent intent = new Intent(this, PatientActivity.class);
            intent.putExtra("username", username);
            intent.putExtra("password", password);
            startActivity(intent);
        }
    }

    public void signInClick(View view){
        String username = ((EditText)findViewById(R.id.username)).getText().toString();
        String password = ((EditText)findViewById(R.id.password)).getText().toString();

        boolean usernameOk = false;
        boolean passwordOk = false;

        TextView incorrectUsernameMessage = findViewById(R.id.wrongUsername);
        if(username.isEmpty()){
            incorrectUsernameMessage.setText("Username cannot be empty");
            incorrectUsernameMessage.setVisibility(View.VISIBLE);
        } else {
            incorrectUsernameMessage.setVisibility(View.INVISIBLE);
            usernameOk = true;
        }

        TextView incorrectPasswordMessage = findViewById(R.id.wrongPassword);
        if(username.isEmpty()){
            incorrectPasswordMessage.setText("Password cannot be empty");
            incorrectPasswordMessage.setVisibility(View.VISIBLE);
        } else {
            incorrectPasswordMessage.setVisibility(View.INVISIBLE);
            passwordOk =  true;
        }

        if(usernameOk && passwordOk){
            TextView loginMessage = findViewById(R.id.loginMessage);
            if(true){
                final ProgressBar progressBar = findViewById(R.id.progressBar2);
                progressBar.setVisibility(View.VISIBLE);
                CountDownTimer progressTimer = new CountDownTimer(4000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        progressBar.setVisibility(View.INVISIBLE);
                        Intent intent = new Intent(MainActivity.this, PatientActivity.class);
                        intent.putExtra("username", username);
                        intent.putExtra("password", password);
                        startActivity(intent);
                        SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                        editor.putBoolean("logged", true);
                        editor.putString("username", username);
                        editor.putString("password", password);
                        editor.apply();
                    }
                }.start();

            } else {
                loginMessage.setText("Login failed. Username or Password are incorrect.");
                loginMessage.setTextColor(Color.RED);
            }
            loginMessage.setVisibility(View.VISIBLE);
        }
    }
}
