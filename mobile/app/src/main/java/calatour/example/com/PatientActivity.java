package calatour.example.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PatientActivity extends AppCompatActivity {

    private boolean notifications = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setAdapter(new Adapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        Button search = findViewById(R.id.searchButton);
        Button appointment = findViewById(R.id.makeAppointment);
        Button history = findViewById(R.id.history);
        search.setOnClickListener(e -> {
            showSearchDialog();
        });
        appointment.setOnClickListener(e -> {
            showAppointmentDialog();
        });
        history.setOnClickListener(e -> {
            showHistoryDialog();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.username);
        menuItem.setTitle(getIntent().getStringExtra("username"));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        //sign out operation
        if (id == R.id.logoff) {
            logout();
            return true;

        } else if (id == R.id.delete) {
            deleteData();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to log out?")
                .setTitle("Log out");

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        builder.setPositiveButton("confirm", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                editor.putBoolean("logged", false);
                editor.apply();
                Intent intent = new Intent(PatientActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAppointmentDialog() {
        AppointmentDialog cdd=new AppointmentDialog(this);
        cdd.show();
    }

    private void showHistoryDialog() {
        HistoryDialog cdd=new HistoryDialog(this);
        cdd.show();
    }

    private void showSearchDialog() {
        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText("Paracetamol");

        new AlertDialog.Builder(this)
                .setTitle("Type the name of the medication you are searching")
                .setView(input)
                .setPositiveButton("Search", (dialog, which) -> {
                    SearchDialog cdd=new SearchDialog(this);
                    cdd.show();
                })
                .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                .show();
    }

    private void deleteData() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please introduce your password");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String introducedPassword = input.getText().toString();
                checkForIntroducedPassword(introducedPassword);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void checkForIntroducedPassword(String introducedPassword){
        String password = getIntent().getStringExtra("password");
        /*System.out.println("Password: " + password + "Introduced: "+introducedPassword);*/
        if (introducedPassword.equals(password)) {
            final ProgressBar progressBar = findViewById(R.id.progressBar);
            LinearLayout layout = findViewById(R.id.mainLayout);
            clearScreen();
            progressBar.setVisibility(View.VISIBLE);
            CountDownTimer progressTimer = new CountDownTimer(4000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(PatientActivity.this, "Data was deleted successfully", Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = getSharedPreferences("Login", MODE_PRIVATE).edit();
                    editor.putBoolean("logged", false);
                    editor.apply();
                    Intent intent = new Intent(PatientActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }.start();

        } else {
            Toast.makeText(this, "Incorrect password", Toast.LENGTH_LONG).show();
        }
    }

    public void handleNotifications(View view) {
        Button button = findViewById(R.id.notificationButton);
        if (notifications == true) {
            button.setText("Enable notifications");
            button.setBackgroundColor(Color.rgb(0, 121, 47));
            Toast.makeText(this, "Notifications are disabled now", Toast.LENGTH_SHORT).show();
            notifications = false;
        } else {
            button.setText("Disable notifications");
            button.setBackgroundColor(Color.rgb(190,0,12));
            Toast.makeText(this, "Notifications are enabled now", Toast.LENGTH_SHORT).show();
            notifications = true;
        }
    }

    private void clearScreen(){
        findViewById(R.id.textView).setVisibility(View.INVISIBLE);
        findViewById(R.id.recyclerView).setVisibility(View.INVISIBLE);
        findViewById(R.id.notificationButton).setVisibility(View.INVISIBLE);
        findViewById(R.id.buttonMenuLayout).setVisibility(View.INVISIBLE);
    }

    private class Adapter extends RecyclerView.Adapter<ListViewHolder> {

        private List<Medication> items = new ArrayList<>();

        Adapter() {
            items = new ArrayList<>();
            items.add(new Medication("Paracetamol", "Tablete", "Zilnic, intre orele 8-9 si 18-19", "Paracetamolul poate influenta excretia altor medicamente metabolizate prin ficat, cum ar fi antibioticul cloramfenicol. De asemenea, retineti ca instalarea efectului medicamentelor poate afecta tranzitul intestinal, acesta devenind mai rapid sau mai lent.", R.drawable.paracetamol));
            items.add(new Medication("Nurofen", "Sirop","Zilnic, inainte de pranz", "Nurofen Forte 400 mg drajeuri calmează eficace un spectru larg de dureri acute.", R.drawable.nurofen));
            items.add(new Medication("Vitamina C", "Tablete", "De 3 ori pe zi", "Vitamina C, cunoscuta si sub denumirea de acid ascorbic, este unul dintre cei mai siguri si mai eficienti nutrienti. Printre beneficiile sale se numara protejarea organismului impotriva deficientelor imunitare, a bolilor cardiovasculare, a problemelor prenatale, a bolilor de ochi si chiar impotriva imbatranirii pielii.", R.drawable.vitaminc));
            items.add(new Medication("Moldamin", "Injectabil", "In fiecare marti", "Moldaminul este o penicilina cu actiune prelungita, pentru administrare parenterala. Spectrul sau de actiune este asemanator cu al penicilinei G potasice cristalizate. Datorita solubilitatii sale scazute, Moldamin se resoarbe lent, asigurand o penicilinemie prelungita, dar cu valori joase", R.drawable.moldamin));
        }

        @NonNull
        @Override
        public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
            Medication medication = items.get(position);
            holder.bind(medication);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }


    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        View itemView;

        ListViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        TextView numeMedicatie;
        TextView modDeAdministrare;
        TextView intakeInterval;

        void bind(Medication medication) {
            numeMedicatie = itemView.findViewById(R.id.numeMedicatie);
            modDeAdministrare = itemView.findViewById(R.id.modDeAdministrare);
            intakeInterval = itemView.findViewById(R.id.intakeInterval);
            numeMedicatie.setText(medication.getName());
            modDeAdministrare.setText(medication.getForm());
            intakeInterval.setText(medication.getIntakeInterval());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view){
                    Intent intent = new Intent(PatientActivity.this, MedicationView.class);
                    intent.putExtra("title", medication.getName());
                    intent.putExtra("img", medication.getImage()+"");
                    intent.putExtra("descr", medication.getDescription());

                    System.out.println(medication.getDescription());
                    startActivity(intent);
                }
            });
        }
    }

}