package calatour.example.com;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MedicationView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medication);

        TextView medName = findViewById(R.id.medName);
        medName.setText(getIntent().getStringExtra("title"));
        ImageView medImage = findViewById(R.id.medImage);
        medImage.setImageResource(Integer.parseInt(getIntent().getStringExtra("img")));
        TextView medDescr = findViewById(R.id.medDescription);
        System.out.println(getIntent().getStringExtra("descr"));
        medDescr.setText(getIntent().getStringExtra("descr"));

    }
}
