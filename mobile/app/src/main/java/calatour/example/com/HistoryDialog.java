package calatour.example.com;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class HistoryDialog extends Dialog implements android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes;

    public HistoryDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.appointment_history);
        yes = (Button) findViewById(R.id.btn_yes);
        yes.setOnClickListener(this);

        RecyclerView allergies = findViewById(R.id.allergiesRV);
        RecyclerView history = findViewById(R.id.historyRV);

        List<String> allergiesList = new ArrayList<>();
        allergiesList.add("Polen");
        allergiesList.add("Penicilin");

        List<String> appointmentHistory = new ArrayList<>();
        appointmentHistory.add("31.09.2019 15:00 PM");
        appointmentHistory.add("15.02.2019 10:00 AM");


        allergies.setAdapter(new Adapter(allergiesList));
        allergies.setLayoutManager(new LinearLayoutManager(c.getBaseContext()));

        history.setAdapter(new Adapter(appointmentHistory));
        history.setLayoutManager(new LinearLayoutManager(c.getBaseContext()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    private class Adapter extends RecyclerView.Adapter<HistoryListViewHolder> {

        private List<String> items;

        Adapter(List<String> items) {
            this.items = items;
        }

        @NonNull
        @Override
        public HistoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.string_list_item, parent, false);
            return new HistoryListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull HistoryListViewHolder holder, int position) {
            String s = items.get(position);
            holder.bind(s);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    class HistoryListViewHolder extends RecyclerView.ViewHolder {

        View itemView;

        HistoryListViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        TextView string;

        void bind(String s) {
            string = itemView.findViewById(R.id.simple_string);
            string.setText(s);
        }
    }
}

