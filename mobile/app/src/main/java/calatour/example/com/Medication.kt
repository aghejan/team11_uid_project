package calatour.example.com

data class Medication(val name: String, val form: String, val intakeInterval: String, val description: String, val image: Integer) {
    lateinit var sideEffects: List<String>
    lateinit var similarMedication: String
}