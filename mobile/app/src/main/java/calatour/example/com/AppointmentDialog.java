package calatour.example.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class AppointmentDialog extends Dialog implements android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes;

    public AppointmentDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.appointment_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                displayAppointmentDate();
                break;
            default:
                break;
        }
        dismiss();
    }

    private void displayAppointmentDate(){
        int randomDay = new Random().nextInt((29 - 1) + 1) + 3;
        int randomHour = new Random().nextInt((21 - 14) + 1) + 14;

        new AlertDialog.Builder(getContext())
                .setTitle("Appointment created for "+randomDay+".02.2020, "+randomHour+":00")
                .setPositiveButton("OK", (dialog, which) -> {

                })
                .setNegativeButton("Request another date", (dialog, which) -> {
                    displayAppointmentDate();
                })
                .show();
        dismiss();
    }
}

